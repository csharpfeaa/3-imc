﻿using System;

namespace IMC
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Introduceti greutatea dvs (kg): ");

            if (!double.TryParse(Console.ReadLine(), out double greutate))
            {
                Console.WriteLine("Valoarea introdusa nu este valida.");
                Console.ReadKey();
                return;
            }

            Console.Write("Introduceti inaltimea (m): ");

            if (!double.TryParse(Console.ReadLine(), out double inaltime))
            {
                Console.WriteLine("Valoarea introdusa nu este valida.");
                Console.ReadKey();
                return;
            }

            double bmi = greutate / Math.Pow(inaltime, 2);

            string rezultat = "";

            if (bmi < 15)
            {
                rezultat = "Foarte foarte subnutrit";
            }
            else if (bmi < 16)
            {
                rezultat = "Foarte subnutrit";
            }
            else if (bmi < 18.5)
            {
                rezultat = "Subnutrit";
            }
            else if (bmi < 25)
            {
                rezultat = "Normal";
            }
            else if (bmi < 30)
            {
                rezultat = "Supraponderal";
            }
            else if (bmi < 35)
            {
                rezultat = "Obez moderat";
            }
            else if (bmi < 40)
            {
                rezultat = "Obez sever";
            }
            else if (bmi >= 40)
            {
                rezultat = "Obez foarte sever";
            }

            Console.WriteLine("Rezultat: {0}", rezultat);
            Console.ReadKey();
        }
    }
}